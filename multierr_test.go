// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package errutils

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var warn = NewWarn("warn-msg", nil)

func TestSliceMultiError(t *testing.T) {
	err := &SliceMultiError{}

	assert.Equal(t, "no errors", err.Error(), "wrong Error()")
	assert.Equal(t, nil, err.GetError(1000), "wrong GetError") // This is not guaranteed behaviour
	assert.Equal(t, nil, err.ToImmutable(), "wrong nil.ToImmutable")
	assert.Equal(t, []error(nil), err.Errors, "wrong Errors")
	assert.Equal(t, 0, err.ErrorLen(), "wrong ErrorLen")
	assert.Equal(t, false, err.CheckError(warn), "wrong CheckErr(warn)")
	assert.Equal(t, []error{warn}, err.Errors, "wrong Errors")
	assert.Equal(t, 1, err.ErrorLen(), "wrong ErrorLen")
	assert.Equal(t, true, err.CheckError(errNorm), "wrong CheckErr(err)")
	assert.Equal(t, []error{warn, errNorm}, err.Errors, "wrong Errors")
	assert.Equal(t, 2, err.ErrorLen(), "wrong ErrorLen")
	assert.Equal(t, false, err.CheckError(nil), "wrong CheckErr(nil)")
	assert.Equal(t, []error{warn, errNorm}, err.Errors, "wrong Errors")
	assert.Equal(t, 2, err.ErrorLen(), "wrong ErrorLen")
	assert.Equal(t, err, err.InsertError(0, errCause), "wrong InsertError")
	assert.Equal(t, []error{errCause, warn, errNorm}, err.Errors, "wrong Errors")
	assert.Equal(t, 3, err.ErrorLen(), "wrong ErrorLen")
	assert.Equal(t, warn, err.GetError(1), "wrong GetError")
	assert.Equal(t, "multiple errors occurred: [cause-msg, warn-msg, error-msg]", err.Error(), "wrong Error()")

	imm := err.ToImmutable()
	assert.Equal(t, 3, imm.ErrorLen(), "wrong ErrorLen")
	assert.Equal(t, warn, imm.GetError(1), "wrong GetError")
	assert.Equal(t, "multiple errors occurred: [cause-msg, warn-msg, error-msg]", imm.Error(), "wrong Error()")
	assert.Equal(t, nil, immutableSliceMultiError{nil}.GetError(1000), "wrong GetError") // This is not guaranteed behaviour
}
