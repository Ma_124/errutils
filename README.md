# errutils
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/Ma_124/errutils)](https://goreportcard.com/report/gitlab.com/Ma_124/errutils)
[![CodeCov](https://codecov.io/gl/Ma_124/errutils/branch/master/graph/badge.svg)](https://codecov.io/gl/Ma_124/errutils)
[![Build Status](https://gitlab.com/Ma_124/errutils/badges/master/build.svg)](https://gitlab.com/Ma_124/errutils/commits/master)
[![GoDoc](https://godoc.org/gitlab.com/Ma_124/errutils?status.svg)](https://godoc.org/gitlab.com/Ma_124/errutils)
[![License MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://ma124.js.org/l/MIT/~/2019)

This is a replacement for [`errors`](https://godoc.org/errors), [`github.com/pkg/errors`](https://godoc.org/github.com/pkg/errors) and [`github.com/hashicorp/go-multierror`](https://godoc.org/github.com/hashicorp/go-multierror).

It supports [IDs](https://godoc.org/gitlab.com/Ma_124/errutils#ErrorWithID), [causes](https://godoc.org/gitlab.com/Ma_124/errutils#ErrorWithCause) and collections of errors ([multierrors](https://godoc.org/gitlab.com/Ma_124/errutils#EditableMultiError)).

There's an additional code generating tool in [`cmd/errgen`](cmd/errgen) that generates numeric error constants that implement these interfaces.
