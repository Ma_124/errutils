// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package errutils

import (
	"strconv"
	"strings"
)

// NewWithID creates a new ExtErrorWithID with the specified params. (Tip: use cmd/errgen to generate error constants)
func NewWithID(scope string, id uint16, msg string, isWarn bool, cause error) ExtErrorWithID {
	return errorWithIDAndCause{
		msg:    msg,
		scope:  scope,
		id:     id,
		cause:  cause,
		isWarn: isWarn,
	}
}

// New creates a new error with the given message and cause.
func New(err string, cause error) ErrorWithCauseAndWarningFlag {
	return errorWithCause{
		err:    err,
		cause:  cause,
		isWarn: false,
	}
}

// NewWarn creates a new warning with the given message and cause.
func NewWarn(err string, cause error) ErrorWithCauseAndWarningFlag {
	return errorWithCause{
		err:    err,
		cause:  cause,
		isWarn: true,
	}
}

// Wrap wraps an error with the given cause.
// If err is an instance of ErrorWithID an ErrorWithID is returned.
func Wrap(err error, cause error) ErrorWithCauseAndWarningFlag {
	switch err.(type) {
	case ErrorWithID:
		scope, id := err.(ErrorWithID).ID()
		return &errorWithIDAndCause{
			msg:    err.(ErrorWithID).Message(),
			scope:  scope,
			id:     id,
			cause:  cause,
			isWarn: IsWarn(err),
		}
	}

	return errorWithCause{
		err:    err.Error(),
		cause:  cause,
		isWarn: IsWarn(err),
	}
}

// IsWarn checks whether an error is a warning.
// If err is not an instance of ErrorWithWarningFlag false is returned otherwise the value of err.IsWarn()
func IsWarn(err error) bool {
	switch err.(type) {
	case ErrorWithWarningFlag:
		return err.(ErrorWithWarningFlag).IsWarn()
	default:
		return false
	}
}

const causedBy = " caused by "
const idMsgSep = ": "
const errNrPrefix = "0x"
const fourZeroes = "0000"

// ErrorMsg is an utility function to generate the right Error() string for an ExtErrorWithID
// Example output: 0xABCD: a terrible error occurred caused by EOF
func ErrorMsg(err ExtErrorWithID) string {
	_, idi := err.ID()
	id := strings.ToUpper(strconv.FormatInt(int64(idi), 16))
	id = fourZeroes[:len(fourZeroes)-len(id)] + id

	msg := err.Message()
	cause := err.Cause()

	if cause != nil {
		s := strings.Builder{}
		cause := cause.Error()
		s.Grow(len(errNrPrefix) + len(id) + len(idMsgSep) + len(msg) + len(causedBy) + len(cause))
		s.WriteString(errNrPrefix)
		s.WriteString(id)
		s.WriteString(idMsgSep)
		s.WriteString(msg)
		s.WriteString(causedBy)
		s.WriteString(cause)
		return s.String()
	}

	s := strings.Builder{}
	s.Grow(len(errNrPrefix) + len(id) + len(idMsgSep) + len(msg))
	s.WriteString(errNrPrefix)
	s.WriteString(id)
	s.WriteString(idMsgSep)
	s.WriteString(msg)
	return s.String()
}

type errorWithIDAndCause struct {
	msg    string
	scope  string
	id     uint16
	cause  error
	isWarn bool
}

func (e errorWithIDAndCause) IsWarn() bool {
	return e.isWarn
}

func (e errorWithIDAndCause) Error() string {
	return ErrorMsg(e)
}

func (e errorWithIDAndCause) ID() (string, uint16) {
	return e.scope, e.id
}

func (e errorWithIDAndCause) Message() string {
	return e.msg
}

func (e errorWithIDAndCause) Cause() error {
	return e.cause
}

type errorWithCause struct {
	err    string
	cause  error
	isWarn bool
}

func (e errorWithCause) IsWarn() bool {
	return e.isWarn
}

func (e errorWithCause) Error() string {
	if e.cause != nil {
		s := strings.Builder{}
		cause := e.cause.Error()
		s.Grow(len(e.err) + len(causedBy) + len(cause))
		s.WriteString(e.err)
		s.WriteString(causedBy)
		s.WriteString(cause)
		return s.String()
	}

	return e.err
}

func (e errorWithCause) Cause() error {
	return e.cause
}
