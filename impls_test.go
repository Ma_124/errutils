// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package errutils

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

var errCause = errors.New("cause-msg")
var errNorm = errors.New("error-msg")

func TestErrorWithIDAndCause(t *testing.T) {
	t.Run("with cause", func(t *testing.T) {
		err := NewWithID("test-scope", 0xABCD, "error-msg", false, errCause)
		scope, id := err.ID()

		assert.Equal(t, "test-scope", scope, "wrong scope")
		assert.Equal(t, uint16(0xABCD), id, "wrong id")
		assert.Equal(t, "error-msg", err.Message(), "wrong msg")

		testError(t, err, true, false, true)
	})
	t.Run("without cause", func(t *testing.T) {
		err := NewWithID("test-scope", 0xABCD, "error-msg", false, nil)
		scope, id := err.ID()

		assert.Equal(t, "test-scope", scope, "wrong scope")
		assert.Equal(t, uint16(0xABCD), id, "wrong id")
		assert.Equal(t, "error-msg", err.Message(), "wrong msg")

		testError(t, err, false, false, true)
	})
	t.Run("wrap with cause", func(t *testing.T) {
		err := Wrap(NewWithID("test-scope", 0xABCD, "error-msg", false, nil), errCause)
		testError(t, err, true, false, true)
	})
}

func TestErrorWithCause(t *testing.T) {
	t.Run("new with cause", func(t *testing.T) {
		err := New("error-msg", errCause)
		testError(t, err, true, false, false)
	})
	t.Run("new without cause", func(t *testing.T) {
		err := New("error-msg", nil)
		testError(t, err, false, false, false)
	})
	t.Run("wrap with cause", func(t *testing.T) {
		err := Wrap(errNorm, errCause)
		testError(t, err, true, false, false)
	})
	t.Run("wrap without cause", func(t *testing.T) {
		err := Wrap(errNorm, nil)
		testError(t, err, false, false, false)
	})
	t.Run("new warn with cause", func(t *testing.T) {
		err := NewWarn("error-msg", errCause)
		testError(t, err, true, true, false)
	})
	t.Run("new warn without cause", func(t *testing.T) {
		err := NewWarn("error-msg", nil)
		testError(t, err, false, true, false)
	})
}

func testError(t *testing.T, err ErrorWithCauseAndWarningFlag, cause bool, warn bool, id bool) {
	if id {
		_, ok := err.(ErrorWithID)
		assert.Equal(t, true, ok, "wrong type")

		scope, id := err.(ErrorWithID).ID()

		assert.Equal(t, "test-scope", scope, "wrong scope")
		assert.Equal(t, uint16(0xABCD), id, "wrong id")
		assert.Equal(t, "error-msg", err.(ErrorWithID).Message(), "wrong msg")

	}

	assert.Equal(t, warn, err.IsWarn(), "wrong isWarn")
	if cause {
		assert.Equal(t, errCause, err.Cause(), "wrong cause")
	} else {
		assert.Equal(t, nil, err.Cause(), "wrong cause")
	}
	if id {
		if cause {
			assert.Equal(t, "0xABCD: error-msg caused by cause-msg", err.Error(), "wrong Error()")
		} else {
			assert.Equal(t, "0xABCD: error-msg", err.Error(), "wrong Error()")
		}
	} else {
		if cause {
			assert.Equal(t, "error-msg caused by cause-msg", err.Error(), "wrong Error()")
		} else {
			assert.Equal(t, "error-msg", err.Error(), "wrong Error()")
		}
	}
}

func TestIsWarn(t *testing.T) {
	tests := []struct {
		name string
		err  error
		want bool
	}{
		{
			name: "nil",
			err:  nil,
			want: false,
		},
		{
			name: "interface yes",
			err:  NewWarn("error-msg", nil),
			want: true,
		},
		{
			name: "interface no",
			err:  New("error-msg", nil),
			want: false,
		},
		{
			name: "builtin.error",
			err:  errNorm,
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, IsWarn(tt.err))
		})
	}
}
