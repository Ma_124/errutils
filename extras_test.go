package errutils

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestExitWithCode(t *testing.T) {
	exitedWith := -1
	Exiter = func(code int) {
		exitedWith = code
	}

	ExitWithCode(nil)
	assert.Equal(t, 0, exitedWith)

	ExitWithCode(errNorm)
	assert.Equal(t, 255, exitedWith)

	ExitWithCode(errorWithExitCode{})
	assert.Equal(t, 124, exitedWith)
}

type errorWithExitCode struct{}

func (e errorWithExitCode) Error() string {
	return "error-msg"
}

func (e errorWithExitCode) ExitCode() int {
	return 124
}
