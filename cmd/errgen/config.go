// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/iancoleman/strcase"
	"github.com/kballard/go-shellquote"
	"gitlab.com/Ma_124/errutils"
	"math/rand"
	"strings"
	"time"
)

// Config is the base type for the errors.yml file
type Config struct {
	Name                string        `yaml:"name"`
	LicenseFile         string        `yaml:"license-file"`
	LicenseText         string        `yaml:"license"`
	Errors              []*ConfigErr  `yaml:"errors"`
	Targets             ConfigTargets `yaml:"target,omitempty"`
	TemplateSearchPaths []string      `yaml:"template-search-paths,omitempty"`
	Lock                *ConfigULock  `yaml:"lock"`
}

// ConfigTargets contains target specific configuration
type ConfigTargets struct {
	Go ConfigGo `yaml:"go,omitempty"`
}

// ConfigGo contains configurations for the generated code
type ConfigGo struct {
	Package        string           `yaml:"package"`
	TypeName       string           `yaml:"type"`
	Prefix         string           `yaml:"prefix"`
	DontImportLibs bool             `yaml:"independent,omitempty"`
	Wrappers       []*ConfigWrapper `yaml:"wrappers,omitempty"`
}

// ConfigErr is the type which registers the errors.
// The IDs can be accessed by calling Config.IDByName(Name) because there stored in Config.Lock
type ConfigErr struct {
	Name           string                 `yaml:"name"`
	Message        string                 `yaml:"msg"`
	Warn           bool                   `yaml:"warn,omitempty"`
	Unstandardized map[string]interface{} `yaml:"x,omitempty"`
}

// ConfigWrapper contains a custom method for errors for example to satisfy interfaces
// for example:
//     - sig: ExitCode() int
//       impl: <cfg exitCode "exit-code" int 2>return exitCode
type ConfigWrapper struct {
	Signature string   `yaml:"sig"`
	Packages  []string `yaml:"pkgs,omitempty"`
	Impl      string   `yaml:"impl"`
}

// ConfigULock stores the IDs for errors and is normally base64-encoded json in errors.yml
type ConfigULock struct {
	IDs   map[string]uint16 `json:"ids"`
	Time  int64             `json:"-"`
	names map[uint16]string
}

// NewName generates a name from the given prefix, suffix and Config.Name that follows go's style guide
func (cfg *Config) NewName(prefix, suffix string, exported bool) string {
	s := prefix + "_" + cfg.Name + "_Error_" + suffix
	if exported {
		return strcase.ToCamel(s)
	}
	return strcase.ToLowerCamel(s)
}

// IDByName returns the numeric ID of an error by it's name
func (cfg *Config) IDByName(name string) uint16 {
	name = strings.ToUpper(name)

	if id, ok := cfg.Lock.IDs[strings.ToUpper(name)]; ok {
		return id
	}

	for {
		id := uint16(1 + rand.Int31n(0xFFFF))
		if _, ok := cfg.Lock.names[id]; !ok {
			cfg.Lock.names[id] = name
			cfg.Lock.IDs[name] = id
			return id
		}
	}
}

// IDStrByName returns the numeric ID of an error by it's name formatted as a string. (fmt string: 0x%0.4X)
func (cfg *Config) IDStrByName(name string) string {
	return fmt.Sprintf("0x%0.4X", cfg.IDByName(name))
}

// LicenseTextCommented returns the value of Config.LicenseText which each line prefixed by a //
func (cfg *Config) LicenseTextCommented() string {
	return "// " + strings.Replace(cfg.LicenseText, "\n", "\n// ", -1)
}

// UnmarshalBinary unmarshals the base64-encoded JSON.
// It can't be UnmarshalJSON or UnmarshalText because it calls json.Unmarshal with itself as a parameter.
func (lock *ConfigULock) UnmarshalBinary(data []byte) error {
	lockData, err := base64.StdEncoding.DecodeString(strings.Replace(string(data), "\n", "", -1))
	if err != nil {
		return err
	}

	err = json.Unmarshal(lockData, lock)
	if err != nil {
		return err
	}

	if lock.IDs == nil {
		lock.IDs = make(map[string]uint16)
	}

	lock.names = make(map[uint16]string, len(lock.IDs))

	for n, id := range lock.IDs {
		lock.names[id] = n
	}

	return nil
}

// Imports returns a set of unique imports build from the parameters and those required by wrappers
func (cfg *ConfigGo) Imports(stds ...string) []string {
	gpkgs := make(map[string]struct{}, len(stds))
	for _, pkg := range stds {
		gpkgs[pkg] = struct{}{}
	}
	for _, wrap := range cfg.Wrappers {
		for _, pkg := range wrap.Packages {
			gpkgs[pkg] = struct{}{}
		}
	}
	pkgs := make([]string, 0, len(gpkgs))
	for pkg := range gpkgs {
		pkgs = append(pkgs, pkg)
	}
	return pkgs
}

// ImplCode parses the ConfigWrapper.Impl field and builds real code from it.
// The meta block starts with a < (U+003C) and ends with a > (U+003E).
// There can be multiple instructions in a meta block separated by ; (U+003B).
// An instruction consists of multiple space separated strings where the first is treated as the instruction name.
// Currently there is only one instruction which populates a variable with the value from the x-section in the config: cfg <var-name> [cfg-name] [typ-name] [def-value]
// NOTE: This method panics if an error occurs.
func (cfg *ConfigWrapper) ImplCode(td *TemplateData) string {
	if !strings.HasPrefix(cfg.Impl, "<") {
		return cfg.Impl
	}

	parts := strings.SplitN(cfg.Impl[1:], ">", 2)
	if len(parts) != 2 {
		panic(ErrSplitMeta)
	}

	insts := strings.Split(parts[0], ";")

	s := &strings.Builder{}

	for _, errs := range td.Config.Errors {
		if errs.Unstandardized == nil {
			errs.Unstandardized = map[string]interface{}{}
		}
	}

	for _, inst := range insts {
		args, err := shellquote.Split(inst)
		if err != nil {
			panic(errutils.Wrap(ErrSplitMeta, err))
		}
		switch args[0] {
		case "cfg":
			cfg.doCfgImpl(args, td, s)
		default:
			panic(ErrUnknownMeta)
		}
	}

	s.WriteString(parts[1])
	return s.String()
}

func (cfg *ConfigWrapper) doCfgImpl(args []string, td *TemplateData, s *strings.Builder) {
	if len(args) <= 1 {
		panic(errutils.Wrap(ErrMetaErr, ErrMetaConfigArgLen))
	}
	var varName string
	var cfgName string
	var typName string
	var varDef string
	if len(args) == 2 {
		varName = strcase.ToLowerCamel(args[1])
		cfgName = args[1]
		typName = "interface{}"
	} else {
		varName = args[1]
		cfgName = args[2]

		if len(args) >= 4 {
			typName = args[3]
		}

		if len(args) >= 5 {
			varDef = args[4]
		}

		if len(args) > 5 {
			panic(errutils.Wrap(ErrMetaErr, ErrMetaConfigArgLen))
		}
	}
	templ, err := lookupTemplate(td.Config, "go-switch-meta")
	if err != nil {
		panic(errutils.Wrap(ErrMetaErr, errutils.Wrap(ErrLoadTemplate, err)))
	}
	err = templ.Execute(s, map[string]interface{}{
		"td":      td,
		"varName": varName,
		"cfgName": cfgName,
		"typName": typName,
		"varDef":  varDef,
	})
	if err != nil {
		panic(errutils.Wrap(ErrMetaErr, errutils.Wrap(ErrExecuteTemplate, err)))
	}
}

// UnmarshalYAML is wrapper around UnmarshalBinary
func (lock *ConfigULock) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var s string
	err := unmarshal(&s)
	if err != nil {
		return err
	}
	return lock.UnmarshalBinary([]byte(s))
}

// MarshalBinary marshals the ConfigULock as JSON and encodes it as base64
func (lock *ConfigULock) MarshalBinary() ([]byte, error) {
	lock.Time = time.Now().Unix()
	data, err := json.Marshal(lock)
	if err != nil {
		return nil, err
	}

	return insertInto(base64.StdEncoding.EncodeToString(data), 118, '\n'), nil
}

// MarshalYAML is wrapper around MarshalBinary
func (lock *ConfigULock) MarshalYAML() (interface{}, error) {
	s, err := lock.MarshalBinary()
	return string(s), err
}

func insertInto(s string, interval int, sep rune) []byte {
	var buffer bytes.Buffer
	before := interval - 1
	last := len(s) - 1
	for i, char := range s {
		buffer.WriteRune(char)
		if i%interval == before && i != last {
			buffer.WriteRune(sep)
		}
	}
	return buffer.Bytes()
}
