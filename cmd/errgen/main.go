// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package main

import (
	cryptorand "crypto/rand"
	"encoding/binary"
	"errors"
	"fmt"
	"gitlab.com/Ma_124/errutils"
	"gopkg.in/alecthomas/kingpin.v2"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"math/rand"
	"os"
	"os/exec"
	"path/filepath"
	"text/template"
	"time"
)

var (
	app              = kingpin.New("errgen", "Generate error constants.")
	goGen            = app.Command("gen", "Generate for target go.")
	goGenErrFile     = goGen.Flag("file", "errors.yml file").Short('f').Default("errors.yml").String()
	goGenOutFile     = goGen.Flag("out", "generated file").Short('o').String()
	goGenSearchPaths = goGen.Flag("include", "add template search paths").Short('I').ExistingFilesOrDirs()

)

func init() {
	var b [8]byte
	_, err := cryptorand.Read(b[:])
	if err != nil {
		panic(ErrSeedRand)
	} else {
		rand.Seed(int64(binary.LittleEndian.Uint64(b[:])))
	}
}

func main() {
	var err error

	switch kingpin.MustParse(app.Parse(os.Args[1:])) {
	case goGen.FullCommand():
		err = goGenF("go")
	}

	errutils.ExitWithCode(err)
}

func goGenF(target string) error {
	goGenTestFile := ""

	if *goGenOutFile == "" {
		if target == "go" {
			*goGenOutFile = "errors_gen.go"
			goGenTestFile = "errors_gen_test.go"
		} else {
			return ErrSpecOut
		}
	}

	var configData []byte
	configData, err := ioutil.ReadFile(*goGenErrFile)
	if err != nil {
		return errutils.Wrap(ErrReadConfig, err)
	}
	cfg := &Config{}
	err = yaml.Unmarshal(configData, cfg)
	if err != nil {
		return errutils.Wrap(ErrParseConfig, err)
	}
	if cfg.Lock == nil {
		cfg.Lock = &ConfigULock{
			IDs:   make(map[string]uint16, len(cfg.Errors)),
			names: make(map[uint16]string, len(cfg.Errors)),
		}
	}
	if cfg.LicenseFile != "" {
		licData, err := ioutil.ReadFile(cfg.LicenseFile)
		if err != nil {
			return errutils.Wrap(ErrReadLicenseFile, err)
		}
		if len(licData) > 2 && licData[len(licData)-1] == '\n' {
			if licData[len(licData)-2] == '\r' {
				licData = licData[:len(licData)-2]
			} else {
				licData = licData[:len(licData)-1]
			}
		}
		cfg.LicenseText = string(licData)
	}

	err = func() (err error) {
		defer func() {
			perr := recover()
			if perr != nil {
				err = fmt.Errorf("%v", perr)
			}
		}()

		err = gen(target, cfg, *goGenOutFile)
		return err
	}()
	if err != nil {
		return errutils.Wrap(ErrGenErr, err)
	}
	if goGenTestFile != "" {
		err = func() (err error) {
			defer func() {
				perr := recover()
				if perr != nil {
					err = fmt.Errorf("%v", perr)
				}
			}()

			err = gen("go_test", cfg, goGenTestFile)
			return err
		}()
		if err != nil {
			return errutils.Wrap(ErrGenErr, err)
		}
	}

	if target == "go" {
		args := []string{"-w", "-s", *goGenOutFile}
		if goGenTestFile != "" {
			args = append(args, goGenTestFile)
		}
		_ = exec.Command("gofmt", args...).Run()
	}

	cfgEncData, err := yaml.Marshal(cfg)
	if err != nil {
		return errutils.Wrap(ErrMarshalConfig, err)
	}
	err = ioutil.WriteFile("errors.yml", cfgEncData, 0660)
	if err != nil {
		return errutils.Wrap(ErrWriteConfig, err)
	}

	return err
}

// TemplateData is the data passed to templates as dot
type TemplateData struct {
	Config *Config
	Time   string
}

// ErrorMsgString contains the source code of ErrorMsg()
func (d *TemplateData) ErrorMsgString() string {
	// TODO script to keep up to date with impls.go
	return `
	_, id_ := err.ID()
	id := strings.ToUpper(strconv.FormatInt(int64(id_), 16))
	id = fourZeroes[:len(fourZeroes)-len(id)] + id

	msg := err.Message()
	cause := err.Cause()

	if cause != nil {
		s := strings.Builder{}
		cause := cause.Error()
		s.Grow(len(errNrPrefix) + len(id) + len(idMsgSep) + len(msg) + len(causedBy) + len(cause))
		s.WriteString(errNrPrefix)
		s.WriteString(id)
		s.WriteString(idMsgSep)
		s.WriteString(msg)
		s.WriteString(causedBy)
		s.WriteString(cause)
		return s.String()
	}

	s := strings.Builder{}
	s.Grow(len(errNrPrefix) + len(id) + len(idMsgSep) + len(msg))
	s.WriteString(errNrPrefix)
	s.WriteString(id)
	s.WriteString(idMsgSep)
	s.WriteString(msg)
	return s.String()
`
}

func gen(target string, cfg *Config, out string) error {
	templ, err := lookupTemplate(cfg, target)
	if err != nil {
		return err
	}
	f, err := os.Create(out)
	if err != nil {
		return errutils.Wrap(ErrWriteOut, err)
	}
	defer f.Close()

	err = templ.Execute(f, &TemplateData{
		Config: cfg,
		Time:   time.Now().Format(time.RFC822),
	})
	if err != nil {
		return errutils.Wrap(ErrExecuteTemplate, err)
	}
	return nil
}

func lookupTemplate(cfg *Config, target string) (*template.Template, error) {
	var fn string
	for _, path := range append(*goGenSearchPaths, cfg.TemplateSearchPaths...) {
		tfn := filepath.Join(path, target+".gotmpl")
		if _, err := os.Stat(tfn); err == nil {
			fn = tfn
			break
		}
	}
	if fn == "" {
		tfn := filepath.Join(".", target+".gotmpl")
		if _, err := os.Stat(tfn); err == nil {
			fn = tfn
		} else {
			tfn := filepath.Join("templates", target+".gotmpl")
			if _, err := os.Stat(tfn); err == nil {
				fn = tfn
			}
		}
	}
	if fn == "" {
		return nil, errutils.Wrap(ErrTemplSearch, errors.New("no such template: "+target))
	}
	templ, err := template.New(target + ".gotmpl").ParseFiles(fn)
	if err != nil {
		return nil, errutils.Wrap(ErrLoadTemplate, err)
	}

	return templ, nil
}
