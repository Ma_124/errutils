// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package errutils

// ErrorWithID extends the builtin error to include an ID
type ErrorWithID interface {
	// Error should normally include the ID and message
	Error() string

	// ID returns the scope of the ID and the ID itself
	ID() (string, uint16)

	// Message returns the error message
	Message() string
}

// ErrorWithCause extends the builtin error to include a cause
type ErrorWithCause interface {
	// Error returns the error message
	Error() string

	// Cause returns the error that caused this one
	Cause() error
}

// ErrorWithWarningFlag extends the builtin and adds the possibility that this is a warning
// For testing whether something is a warning use errutils.IsWarn()
type ErrorWithWarningFlag interface {
	// Error returns the error message
	Error() string

	// IsWarn returns whether this is a warning.
	// To test this use errutils.IsWarn()
	IsWarn() bool
}

// ErrorWithCauseAndWarningFlag extends the builtin error to include a cause and adds the possibility that this is a warning
type ErrorWithCauseAndWarningFlag interface {
	// Error returns the error message
	Error() string

	// Cause returns the error that caused this one
	Cause() error

	// IsWarn returns whether this is a warning.
	// To test this use errutils.IsWarn()
	IsWarn() bool
}

// ExtErrorWithID extends the builtin error to include an ID and a cause and adds the possibility that this is a warning
type ExtErrorWithID interface {
	// Error should normally include the ID and message
	Error() string

	// ID returns the scope of the ID and the ID itself
	ID() (string, uint16)

	// Message returns the error message
	Message() string

	// Cause returns the error that caused this one
	Cause() error

	// IsWarn returns whether this is a warning.
	// To test this use errutils.IsWarn()
	IsWarn() bool
}

// MultiError is a collection of multiple errors and represents them by a single object
type MultiError interface {
	// Error returns the error messages of each individual error
	Error() string

	// ErrorLen returns the number of errors
	ErrorLen() int

	// GetError returns the error at the specified index
	GetError(i int) error
}

// EditableMultiError collects multiple errors and represents them by a single object
type EditableMultiError interface {
	// Error returns the error messages of each individual error
	Error() string

	// ErrorLen returns the number of errors
	ErrorLen() int

	// GetError returns the error at the specified index
	GetError(i int) error

	// InsertError inserts an error at the specified index
	// If i is greater than ErrorLen() or smaller than zero it's appended to the end
	InsertError(i int, err error) EditableMultiError

	// CheckError checks whether the err is fatal
	// Calls to CheckError(nil) are ignored return false otherwise errutils.IsWarn() is returned and the error appended to the list of errors
	CheckError(err error) bool
}
