package errutils

import (
	"fmt"
	"os"
)

// Exiter is called by ExitWithCode and it's default is os.Exit
var Exiter = os.Exit

// ErrorWithExitCode extends the builtin error type to have a exit code associated with it.
// To get the ExitCode use errutils.ExitCode() or exit with errutils.ExitWithCode() directly
type ErrorWithExitCode interface {
	Error() string
	ExitCode() int
}

// ExitCode returns err.ExitCode() if err implements ExitCode and def otherwise
func ExitCode(err error, def int) int {
	if err == nil {
		return 0
	}
	if aerr, ok := err.(ErrorWithExitCode); ok {
		return aerr.ExitCode()
	}
	return def
}

// ExitWithCode prints err.Error() if its non nil and exits with the code determined by ExitCode or 255 otherwise
func ExitWithCode(err error) {
	if err != nil {
		fmt.Println(err.Error())
	}
	Exiter(ExitCode(err, 255))
}

// ErrorWithHTTPStatus extends the builtin error type with HTTP Status code
type ErrorWithHTTPStatus interface {
	Error() string
	HTTPStatus() int
}

// ErrorWithPublicMessage extends the builtin error type with a public message
// If Error() != PublicError() PublicError() can be safely returned by a webservice for example while Error() should only be returned encrypted or written to a log.
type ErrorWithPublicMessage interface {
	Error() string
	PublicError() string
}
