// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package errutils

import (
	"strings"
)

// SliceMultiError is an EditableMultiError which stores the errors as a slice
type SliceMultiError struct {
	// Errors holds all the errors represented by this MultiError.
	Errors []error
}

// Error returns the error messages of each individual error
func (m *SliceMultiError) Error() string {
	return sliceErrString(m.Errors)
}

// ErrorLen returns the number of errors (same as len(Errors))
func (m *SliceMultiError) ErrorLen() int {
	return len(m.Errors)
}

// GetError returns the error at the specified index (same as m.Errors[i])
func (m *SliceMultiError) GetError(i int) error {
	if m.Errors == nil {
		// TODO panic?
		return nil // This is not guaranteed behaviour
	}
	return m.Errors[i]
}

// InsertError inserts an error at the specified index
// If i is greater than ErrorLen() or smaller than zero it's appended to the end
func (m *SliceMultiError) InsertError(i int, err error) EditableMultiError {
	if m.Errors == nil {
		m.Errors = []error{err}
		return m
	}

	if i < 0 || i >= len(m.Errors) {
		m.Errors = append(m.Errors, err)
		return m
	}

	m.Errors = append(m.Errors, nil)
	copy(m.Errors[i+1:], m.Errors[i:])
	m.Errors[i] = err

	return m
}

// CheckError checks whether the err is fatal
// Calls to CheckError(nil) are ignored return false otherwise errutils.IsWarn() is returned and the error appended to the list of errors
func (m *SliceMultiError) CheckError(err error) bool {
	if err == nil {
		return false
	}
	_ = m.InsertError(-1, err)
	return !IsWarn(err)
}

// ToImmutable returns an immutable MultiError.
// The Errors slice is copied.
func (m *SliceMultiError) ToImmutable() MultiError {
	if len(m.Errors) == 0 {
		return nil
	}

	newErrs := make([]error, len(m.Errors))
	copy(newErrs, m.Errors)
	return immutableSliceMultiError{
		Errors: newErrs,
	}
}

type immutableSliceMultiError struct {
	Errors []error
}

func (m immutableSliceMultiError) Error() string {
	return sliceErrString(m.Errors)
}

func (m immutableSliceMultiError) ErrorLen() int {
	return len(m.Errors)
}

func (m immutableSliceMultiError) GetError(i int) error {
	if m.Errors == nil {
		// TODO panic?
		return nil // This is not guaranteed behaviour
	}
	return m.Errors[i]
}

const multipleErrors = "multiple errors occurred: ["

func sliceErrString(errs []error) string {
	if errs == nil {
		return "no errors"
	}

	s := strings.Builder{}
	// TODO maybe add s.Grow()
	s.WriteString(multipleErrors)
	for i, err := range errs {
		if i != 0 {
			s.WriteString(", ")
		}
		s.WriteString(err.Error())
	}
	s.WriteString("]")
	return s.String()
}
